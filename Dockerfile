FROM alpine:3.16
RUN addgroup -g 101 -S nginx && \
    adduser -S -D -H -u 101 -h /var/cache/nginx -s /sbin/nologin -G nginx -g nginx nginx && \
    apk add --no-cache \
        g++ \
        make \
        pcre-dev \
        zlib-dev \
        git \
        openssl-dev \
        tzdata \
        ca-certificates && \
    cd /tmp && \
    wget https://github.com/nginx/nginx/archive/refs/tags/release-1.22.1.tar.gz && \
    tar xvf release-1.22.1.tar.gz && \
    rm release-1.22.1.tar.gz && \
    mkdir /usr/src && \
    cd /usr/src && \
    # git clone https://github.com/Refinitiv/nginx-sticky-module-ng.git && \
    git clone https://github.com/vozlt/nginx-module-vts.git && \
    cd /tmp/nginx-release-1.22.1 && \
    ./auto/configure \
        --prefix=/etc/nginx \
        --sbin-path=/usr/sbin/nginx \
        --modules-path=/usr/lib/nginx/modules \
        --conf-path=/etc/nginx/nginx.conf \
        --error-log-path=/var/log/nginx/error.log \
        --http-log-path=/var/log/nginx/access.log \
        --user=nginx \
        --group=nginx \
        --with-http_stub_status_module \
        --with-compat \
        --with-threads \
        --with-http_addition_module \
        --with-http_auth_request_module \
        --with-http_dav_module \
        --with-http_flv_module \
        --with-http_gunzip_module \
        --with-http_gzip_static_module \
        --with-http_mp4_module \
        --with-http_random_index_module \
        --with-http_realip_module \
        --with-http_secure_link_module \
        --with-http_slice_module \
        --with-http_ssl_module \
        --with-http_stub_status_module \
        --with-http_sub_module \
        --with-http_v2_module \
        --with-mail \
        --with-mail_ssl_module \
        --with-stream \
        --with-stream_realip_module \
        --with-stream_ssl_module \
        --with-stream_ssl_preread_module \
        # --add-module=/usr/src/nginx-sticky-module-ng \
        --add-module=/usr/src/nginx-module-vts && \
    make && \
    make install && \
    chown -R nginx:nginx /etc/nginx && \
    chown -R nginx:nginx /var/log/nginx && \
    apk del \
        g++ \
        make \
        git && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log
# USER nginx
CMD ["/usr/sbin/nginx", "-g", "daemon off;"]
